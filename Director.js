export class CoffeeDirector {
  constructor(coffeeBuilder) {
    this.builder = coffeeBuilder;
  }

  makeCaffeLatte = (size) => {
    this.builder.setName('Caffe Latte');
    this.builder.setIngredient('Steamed milk', '50%');
    this.builder.setIngredient('Expresso', '30%');
    this.builder.setIngredient('Milk foam', '20%');
    this.builder.setSize(size || 'Small');
    return this.builder.buildCoffee();
  };

  makeEspresso = (size) => {
    this.builder.setName('Espresso');
    this.builder.setIngredient('Expresso', '30%');
    this.builder.setSize(size || 'Small');
    return this.builder.buildCoffee();
  };

  makeMocha = (size) => {
    this.builder.setName('Caffé Mocha');
    this.builder.setIngredient('Whipped cream', '20%');
    this.builder.setIngredient('Steamed milk', '30%');
    this.builder.setIngredient('Expresso', '30%');
    this.builder.setIngredient('Chocolate syrup', '20%');
    this.builder.setSize(size || 'Small');
    return this.builder.buildCoffee();
  };

  makeMacchiato = (size) => {
    this.builder.setName('Latte Machiatto');
    this.builder.setIngredient('Steamed milk', '80%');
    this.builder.setIngredient('Espresso', '20%');
    this.builder.setSize(size || 'Venti');
    return this.builder.buildCoffee();
  };
}
