import { Coffee } from './Builder.js';
import { CoffeeDirector } from './Director.js';

let size = '';

document.getElementById('latte').addEventListener('click', () => {
  const Latte = new CoffeeDirector(new Coffee()).makeCaffeLatte(size);
  Latte.getInfo();
  Latte.getString();
});
document.getElementById('espresso').addEventListener('click', () => {
  const Espresso = new CoffeeDirector(new Coffee()).makeEspresso(size);
  Espresso.getInfo();
  Espresso.getString();
});
document.getElementById('mocha').addEventListener('click', () => {
  const Mocha = new CoffeeDirector(new Coffee()).makeMocha(size);
  Mocha.getInfo();
  Mocha.getString();
});
document.getElementById('macchiato').addEventListener('click', () => {
  const Macchiato = new CoffeeDirector(new Coffee()).makeMacchiato(size);
  Macchiato.getInfo();
  Macchiato.getString();
});

document.getElementById('small').addEventListener('click', (event) => {
  document.getElementsByClassName('selected')[0].className = '';
  event.target.className = 'selected';
  size = 'small';
});
document.getElementById('large').addEventListener('click', (event) => {
  document.getElementsByClassName('selected')[0].className = '';
  event.target.className = 'selected';
  size = 'large';
});
document.getElementById('venti').addEventListener('click', (event) => {
  document.getElementsByClassName('selected')[0].className = '';
  event.target.className = 'selected';
  size = 'venti';
});
