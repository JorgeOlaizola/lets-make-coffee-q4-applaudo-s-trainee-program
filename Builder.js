export class Ingredient {
  constructor(name, amount) {
    (this.name = name), (this.amount = amount);
  }
}

export function Coffee() {
  (this.name = undefined), (this.size = undefined), (this.ingredients = []);
  // getInfo = () => {
  //     console.log(this)
  // }
}

Coffee.prototype.setName = function (name) {
  this.name = name;
  return this;
};

Coffee.prototype.setSize = function (size) {
  this.size = size;
  return this;
};

Coffee.prototype.setIngredient = function (name, amount) {
  this.ingredients.push(new Ingredient(name, amount));
  return this;
};

Coffee.prototype.getInfo = function () {
  console.log(this);
};

Coffee.prototype.getString = function () {
  console.log(`Your order is a ${this.size.toLowerCase()} ${this.name}`);
};

Coffee.prototype.buildCoffee = function () {
  return this;
};
